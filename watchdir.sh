#!/bin/bash

if ! [ $SSH_AGENT_PID ]
then
  echo "no ssh-agent running..." && exit 1
fi

if ! [ $MYLOCALWEB ]
then
  echo "no local web host set.." && exit 1
fi

if ! [ $MYREMOTEWEB ]
then
  echo "no remote web host set.." && exit 1
fi

WATCHDIR=$HOME/web

while true; do
  inotifywait -r \
    -e create \
    -e modify \
    -e delete \
    -e move   \
    $WATCHDIR && \
    rsync -e ssh -varzn $WATCHDIR $MYLOCALWEB
    rsync -e ssh -varzn $WATCHDIR/ $MYREMOTEWEB
done
